// GLOBAL
function DOM_ID(id) {
  return document.getElementById(id);
}

// BÀI 1
document.querySelector("#v-pills-bai1 button").onclick = function () {
  var diemChuan = Number(DOM_ID("diemChuanBT1").value);
  if (diemChuan <= 0) {
    alert("Vui lòng nhập điểm chuẩn hợp lệ!");
    return;
  }
  var array = document.querySelectorAll("#diemThiSinhBT1 input");
  var tongDiemMon = 0;
  var checkDiem0 = 0;
  var result = "";
  for (var i = 0; i < array.length; i++) {
    tongDiemMon += Number(array[i].value);
    if (Number(array[i].value) == 0) {
      checkDiem0++;
    }
  }
  var diemKhuVuc = Number(DOM_ID("khuVucBT1").value);
  var diemDoiTuong = Number(DOM_ID("doiTuongBT1").value);
  var tongDiem = tongDiemMon + diemKhuVuc + diemDoiTuong;
  if (tongDiem >= diemChuan && checkDiem0 == 0) {
    result = "Bạn đã đậu!";
  } else {
    result = "Bạn đã rớt!";
  }
  DOM_ID("tongDiemBT1").innerText = tongDiem;
  DOM_ID("ketQuaBT1").innerText = result;
};

// BÀI 2
document.querySelector("#v-pills-bai2 button").onclick = function () {
  var resultKhachHang = DOM_ID("khachHangBT2").value;
  DOM_ID("resultKhachHangBT2").innerText = resultKhachHang;
  var soKw = DOM_ID("soKwBT2").value;
  var resultTienDien = "";
  if (soKw == "" || soKw * 1 < 0) {
    resultTienDien = "Vui lòng nhập thông tin hợp lệ!";
  } else {
    var tienDien = 0;
    soKw *= 1;
    if (soKw <= 50) {
      tienDien = tinhTien50KwDau(soKw);
    } else if (soKw > 50 && soKw <= 100) {
      tienDien = tinhTienHon50_Den100Kw(soKw);
    } else if (soKw > 100 && soKw <= 200) {
      tienDien = tinhTienHon100_Den200Kw(soKw);
    } else if (soKw > 200 && soKw <= 350) {
      tienDien = tinhTienHon200_Den350Kw(soKw);
    } else {
      tienDien = tinhTienHon350Kw(soKw);
    }
    resultTienDien = `${tienDien.toLocaleString()} VND`;
  }
  DOM_ID("resultTienDienBT2").innerText = resultTienDien;
};
function tinhTien50KwDau(x) {
  return x * 500;
}
function tinhTienHon50_Den100Kw(x) {
  return tinhTien50KwDau(50) + (x - 50) * 650;
}
function tinhTienHon100_Den200Kw(x) {
  return tinhTienHon50_Den100Kw(100) + (x - 100) * 850;
}
function tinhTienHon200_Den350Kw(x) {
  return tinhTienHon100_Den200Kw(200) + (x - 200) * 1100;
}
function tinhTienHon350Kw(x) {
  return tinhTienHon200_Den350Kw(350) + (x - 350) * 1300;
}

// BÀI 3
document.querySelector("#v-pills-bai3 button").onclick = function () {
  var resultHoTen = DOM_ID("hoTenBT3").value;
  DOM_ID("resultHoTenBT3").innerText = resultHoTen;
  var tongThuNhap = DOM_ID("tongThuNhapBT3").value;
  var resultSoThueTra = "";
  if (tongThuNhap == "" || tongThuNhap * 1 < 0) {
    resultSoThueTra = "Vui lòng nhập thông tin hợp lệ!";
  } else {
    var soPhuThuoc = Number(DOM_ID("soPhuThuocBT3").value);
    var soChiuThue = tongThuNhap * 1 - 4e6 - soPhuThuoc * 1.6e6;
    var soThueTra = 0;
    if (soChiuThue <= 0) {
      soThueTra = 0;
    } else {
      if (soChiuThue <= 60e6) {
        soThueTra = tinhThueDen60Tr(soChiuThue);
      } else if (soChiuThue > 60e6 && soChiuThue <= 120e6) {
        soThueTra = tinhThueTren60_Den120Tr(soChiuThue);
      } else if (soChiuThue > 120e6 && soChiuThue <= 210e6) {
        soThueTra = tinhThueTren120_Den210Tr(soChiuThue);
      } else if (soChiuThue > 210e6 && soChiuThue <= 384e6) {
        soThueTra = tinhThueTren210_Den384Tr(soChiuThue);
      } else if (soChiuThue > 384e6 && soChiuThue <= 624e6) {
        soThueTra = tinhThueTren384_Den624Tr(soChiuThue);
      } else if (soChiuThue > 624e6 && soChiuThue <= 960e6) {
        soThueTra = tinhThueTren624_Den960Tr(soChiuThue);
      } else {
        soThueTra = tinhThueTren960(soChiuThue);
      }
    }
    resultSoThueTra = `${soThueTra.toLocaleString()} VND`;
  }
  DOM_ID("resultTienThueBT3").innerText = resultSoThueTra;
};
function tinhThueDen60Tr(x) {
  return x * 0.05;
}
function tinhThueTren60_Den120Tr(x) {
  return tinhThueDen60Tr(60e6) + (x - 60e6) * 0.1;
}
function tinhThueTren120_Den210Tr(x) {
  return tinhThueTren60_Den120Tr(120e6) + (x - 120e6) * 0.15;
}
function tinhThueTren210_Den384Tr(x) {
  return tinhThueTren120_Den210Tr(210e6) + (x - 210e6) * 0.2;
}
function tinhThueTren384_Den624Tr(x) {
  return tinhThueTren210_Den384Tr(384e6) + (x - 384e6) * 0.25;
}
function tinhThueTren624_Den960Tr(x) {
  return tinhThueTren384_Den624Tr(624e6) + (x - 624e6) * 0.3;
}
function tinhThueTren960(x) {
  return tinhThueTren624_Den960Tr(960e6) + (x - 960e6) * 0.35;
}

// BÀI 4
DOM_ID("formKetNoiBT4").style.display = "none";
DOM_ID("loaiKhachHangBT4").onchange = function () {
  var loai = DOM_ID("loaiKhachHangBT4").value;
  if (loai == 2) {
    DOM_ID("formKetNoiBT4").style.display = "block";
  } else {
    DOM_ID("formKetNoiBT4").style.display = "none";
    if (loai == 0) {
      DOM_ID("maKhachHangBT4").value = "";
      DOM_ID("soKenhCaoCapBT4").value = "";
      DOM_ID("soKetNoiBT4").value = "";
      DOM_ID("resultMaKHBT4").innerText = "";
      DOM_ID("resultTienCapBT4").innerText = "";
    }
  }
};
document.querySelector("#v-pills-bai4 button").onclick = function () {
  var loai = DOM_ID("loaiKhachHangBT4").value;
  var maKhachHang = DOM_ID("maKhachHangBT4").value;
  var soKenhCaoCap = Number(DOM_ID("soKenhCaoCapBT4").value);
  var soKetNoi = Number(DOM_ID("soKetNoiBT4").value);
  var tienCap = 0;
  if (loai == 0) {
    alert("Vui lòng chọn loại khách hàng!");
    return;
  } else if (loai == 1) {
    tienCap = 4.5 + 20.5 + 7.5 * soKenhCaoCap;
  } else {
    var tienKetNoi = 0;
    if (soKetNoi > 10) {
      tienKetNoi = 75 + (soKetNoi - 10) * 5;
    } else {
      tienKetNoi = 75;
    }
    tienCap = 15 + tienKetNoi + 50 * soKenhCaoCap;
  }
  DOM_ID("resultMaKHBT4").innerText = maKhachHang;
  DOM_ID("resultTienCapBT4").innerText = `$${tienCap.toLocaleString(undefined, {
    minimumFractionDigits: 2,
  })}`;
};
